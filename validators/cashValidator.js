const { InvalidArgumentException } = require('../exceptions/InvalidArgumentException');
const { NoteUnavailableException } = require('../exceptions/NoteUnavailableException');
const { NOTES } = require('../constants');
const { OK, BAD_REQUEST } = require('http-status-codes');

module.exports.cashValidator = async (ctx, next) => {
    if (!ctx.request.body.hasOwnProperty('cash')) {
        ctx.throw(BAD_REQUEST, JSON.stringify({ status: BAD_REQUEST, message: new InvalidArgumentException('You must provide cash.').message }));
        return await next();
    }
    let cash = parseInt(ctx.request.body.cash);
    if (cashIsIncorrect(cash)) {
        ctx.throw(BAD_REQUEST, JSON.stringify({ status: BAD_REQUEST, message: new InvalidArgumentException('Whoops, make sure that provided cash is positive integer.').message }));
        return await next();
    }
    if (cash && cash % Math.min.apply(Math, NOTES) !== 0) {
        ctx.throw(BAD_REQUEST, JSON.stringify({ status: BAD_REQUEST, message: new NoteUnavailableException(`Unfortunately, it\'s impossible to withdraw ${cash} with available bank notes.`).message }));
        return await next();
    }
    return await next();
}

const cashIsIncorrect = cash => cash < 0 || isNaN(cash);
