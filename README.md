## Cash withdawal service

The Problem
Develop a solution that simulate the delivery of notes when a client does a withdraw in a cash machine.

The basic requirements are the follow:

Always deliver the lowest number of possible notes;
It’s possible to get the amount requested with available notes;
The client balance is infinite;
Amount of notes is infinite;
Available notes $ 100,00; $ 50,00; $ 20,00 e $ 10,00
Example:
Entry: 30.00
Result: [20.00, 10.00]

Entry: 80.00
Result: [50.00, 20.00, 10.00]

Entry: 125.00
Result: throw NoteUnavailableException

Entry: -130.00
Result: throw InvalidArgumentException

Entry: NULL
Result: [Empty Set]

The Deliverables:
Make sure your code is well written, focus in good practices and prepare a small (10 minutes) talk/explanation about the decisions in your code/architecture.

The usage of libraries or frameworks is allowed, if this make your test easier or better feel free to make use of those.

With the test please delivery:

Frontend Application ( web, cli, mobile, choose one or more )
Endpoints ( rest api, in-code modules, choose what makes you more comfortable )
Tests
Short Presentation

## How to run it

to start backend, run `npm i && npm start` command in the main directory. Backend is accessible on localhost:3001 URL.
to start frontend, run `npm i && npm start` commaind in the `ui` child of main directory. Frontend is accessible on localhost:3000 URL.
to run tests, issue `npm test` command in the main directory.

Tested on Node v10.15.3 and Chromium browser.