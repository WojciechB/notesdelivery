import React, { Component } from 'react';
import { getWithdrawnNotes } from '../../services/httpService';

export default class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cash: '',
            responseStatus: 200,
            bankNotes: [],
            withdrawnMessage: ''
        }
        this.onCashWithdraw = this.onCashWithdraw.bind(this);
        this.onCashInputChange = this.onCashInputChange.bind(this);
        this.formatNotes = this.formatNotes.bind(this);
    }
    onCashWithdraw(event) {
        getWithdrawnNotes(this.state.cash)
            .then(response => this.setState({ responseStatus: response.status, withdrawnMessage: response.message, bankNotes: response.bankNotes ? response.bankNotes : [] }))
            .catch(error => error);
        event.preventDefault();
    }

    onCashInputChange(event) {
        this.setState({ cash: event.target.value, withdrawnMessage: '' });
    }

    formatNotes() {
        let counts = {};
        this.state.bankNotes.forEach(note => { counts[note] = (counts[note] || 0) + 1; });
        return Object.keys(counts).map(key => <p className='notes-amount'>{counts[key]} of {key}</p>);
    }

    render() {
        return <div className="form">
            <div className="input-wrap">
                <p className="rules-paragraph">Provide the cash amount and click the button to withdraw. As a result, you'll receive the set of 100, 50, 20 and 10 notes that sum up to the value you provided.</p>
                <input type="text" placeholder="Cash amount" id="cash-input" value={this.state.cash} className="form-input" onChange={this.onCashInputChange}></input>
                <button className="withdraw-button" onClick={this.onCashWithdraw}>Withdraw my cash</button>
                {this.state.withdrawnMessage && <p className={this.state.responseStatus === 200 ? 'positive-result' : 'negative-result'}>{this.state.withdrawnMessage}</p>}
                {this.state.withdrawnMessage && this.formatNotes()}
            </div>
        </div>;
    }
} 
