const Router = require('koa-router');
const { withdrawNotes } = require('../services/withdrawNotes');
const { cashValidator } = require('../validators/cashValidator');

const router = new Router();

router.post('/withdrawNotes', cashValidator, async (ctx, next) => await withdrawNotes(ctx, next));

module.exports = router.routes();