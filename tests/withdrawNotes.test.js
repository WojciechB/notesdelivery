let chai = require('chai');
let chaiHttp = require('chai-http');
const { OK, BAD_REQUEST } = require('http-status-codes');
const server = require('../index');
chai.use(chaiHttp);
let should = chai.should();

describe('Cash withdrawal ', () => {
    const contentType = 'application/json';
    it('should respond with bank notes, simple case.', async () => {
        // GIVEN
        let request = { cash: 20 };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(OK);
        response.type.should.equal(contentType);
        response.body.message.should.equal('Great! You\'ve earned Your notes!');
        response.body.bankNotes.should.eql([20]);
    });
    it('should respond with bank notes, a bit more complex case', async () => {
        // GIVEN
        let request = { cash: 180 };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(OK);
        response.type.should.equal(contentType);
        response.body.message.should.equal('Great! You\'ve earned Your notes!');
        response.body.bankNotes.should.eql([100, 50, 20, 10]);
    });
    it('should respond successfully getting correct big cash', async () => {
        // GIVEN
        let request = { cash: 2000100 };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(OK);
        response.type.should.equal(contentType);
        response.body.message.should.equal('Great! You\'ve earned Your notes!');
        response.body.bankNotes.should.contain(100);
        response.body.bankNotes.should.not.contain(50);
        response.body.bankNotes.should.not.contain(20);
        response.body.bankNotes.should.not.contain(10);
        response.body.bankNotes.length.should.equal(20001);
    });
    it('should fail when not providing cash property', async () => {
        // GIVEN
        let request = { test: '123' };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(BAD_REQUEST);
        response.type.should.equal('text/plain');
        response.error.should.exist;
        response.error.text.should.contain('You must provide cash.');
    });
    it('should fail when getting string', async () => {
        // GIVEN
        let request = { cash: 'test' };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(BAD_REQUEST);
        response.type.should.equal('text/plain');
        response.error.should.exist;
        response.error.text.should.contain('Whoops, make sure that provided cash is positive integer.');
    });
    it('should fail when getting command like input', async () => {
        // GIVEN
        let request = { cash: '/rm -rf /' };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(BAD_REQUEST);
        response.type.should.equal('text/plain');
        response.error.should.exist;
        response.error.text.should.contain('Whoops, make sure that provided cash is positive integer.');
    });
    it('should fail when getting negative digit', async () => {
        // GIVEN
        let request = { cash: -100 };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(BAD_REQUEST);
        response.type.should.equal('text/plain');
        response.error.should.exist;
        response.error.text.should.contain('Whoops, make sure that provided cash is positive integer');
    });
    it('should fail when getting cash amount not possible to withdraw', async () => {
        // GIVEN
        let request = { cash: 222 };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(BAD_REQUEST);
        response.type.should.equal('text/plain');
        response.error.should.exist;
        response.error.text.should.contain('Unfortunately, it\'s impossible to withdraw');
    });
    it('should return appropriate message when passing zero', async () => {
        // GIVEN
        let request = { cash: 0 };
        // WHEN
        const response = await chai.request(server).post('/withdrawNotes').send(request);
        // THEN
        response.should.have.status(OK);
        response.type.should.equal(contentType);
        response.body.message.should.equal('No bank notes for no money :)');
    });
    after(() => server.close());
});
