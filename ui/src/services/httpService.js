const API_URL = 'http://localhost:3001';

export const getWithdrawnNotes = cash => {
    return fetch(`${API_URL}/withdrawNotes`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ cash })
    }).then(response => response.json());
}
