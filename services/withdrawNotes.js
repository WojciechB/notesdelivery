const { OK } = require('http-status-codes');
const { NOTES } = require('../constants');

module.exports.withdrawNotes = async (ctx, next) => {
    let cash = ctx.request.body.cash;
    if (cash == 0) {
        ctx.body = { status: OK, message: 'No bank notes for no money :)' }
        ctx.status = OK;
        return await next();
    }
    const bankNotes = [];
    while (cash > 0) {
        let biggestNominal = Math.max.apply(Math, NOTES.filter(note => note <= cash));
        cash -= biggestNominal;
        bankNotes.push(biggestNominal);
    }
    ctx.body = { status: OK, message: 'Great! You\'ve earned Your notes!', bankNotes };
    ctx.status = OK;
    await next();
};