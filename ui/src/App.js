import React from 'react';
import Header from './components/Header/index';
import Body from './components/Body/index';
import './App.css';

function App() {
  return (
    <div className="App">
        <Header />
        <Body />
    </div>
  );
}

export default App;
