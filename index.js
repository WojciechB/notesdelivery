const Koa = require('koa');
const KoaBodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const logger = require('koa-logger');
const routes = require('./routes');
const app = new Koa();

app.use(cors({ origin: '*' }));
app.use(KoaBodyParser());
app.use(logger());
app.use(routes);

const server = app.listen(process.env.PORT);

module.exports = server;
